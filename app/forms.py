from flask_wtf import Form
from wtforms import TextField, TextAreaField, IntegerField, SelectField
from wtforms.validators import DataRequired

class todoForm(Form):
    Title = TextField('Title', validators=[DataRequired()])
    Seasons = IntegerField('No. of Seasons', validators=[DataRequired()])
    Genre = SelectField('Genre', validators=[DataRequired()],
    choices = [('Action',"Action"),('Adventure',"Adventure"),
    ('Sci-Fi',"Sci-Fi"), ('Mystery',"Mystery")])
    Universe = TextField('Universe', validators=[DataRequired()])


class searchForm(Form):
    Genre = SelectField('Genre', validators=[DataRequired()],
    choices = [('Action',"Action"),('Adventure',"Adventure"),
    ('Sci-Fi',"Sci-Fi"), ('Mystery',"Mystery")])

class settingsForm(Form):
    usern = TextField('Title', validators=[DataRequired()])
    passw = TextField('Universe', validators=[DataRequired()])
