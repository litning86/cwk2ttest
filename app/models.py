from app import db

class Todo(db.Model):
    id =db.Column(db.Integer, primary_key=True)
    title = db.Column(db.String(500), index=True, unique=True)
    desc = db.Column(db.String(500), index=True, unique=True)
    date_created = db.Column(db.Date)
    date_completed = db.Column(db.Date)
    complete = db.Column(db.Boolean)

class User(db.Model):
    id =db.Column(db.Integer, primary_key=True)
    username1 = db.Column(db.String(500), index=True, unique=True)
    password1 = db.Column(db.String(500), index=True)

class Tvshows(db.Model):
    id =db.Column(db.Integer, primary_key=True)
    title =db.Column(db.String(69), index=True)
    Seasons =db.Column(db.Integer)
    Genre = db.Column(db.String(50), index=True)
    Universe = db.Column(db.String(100), index=True)

class Medium(db.Model):
    id = db.Column(db.Integer, primary_key=True, unique=True)
    UserId = db.Column(db.Integer, db.ForeignKey(User.id))
    TvshowsId = db.Column(db.Integer, db.ForeignKey(Tvshows.id))

class Genres(db.Model):
    id =db.Column(db.Integer, primary_key=True)
    thing = db.Column(db.String(20), index=True)
