from flask import render_template, redirect, url_for, flash, request, session
from app import app, db, models
from datetime import date
from .forms import todoForm, searchForm, settingsForm
from flask_table import Table, Col
import hashlib

class Results(Table):
    id = Col('Id', show=False)
    title = Col('Title')
    Seasons = Col('Seasons')
    Genre = Col('Genre')
    Universe = Col('Universe Collection')

@app.route('/')
def index():
    print(session.get('logged_in'))
    incomplete = []
    if not session.get('logged_in'):
        return render_template('login.html')
    else:
        user1 = " "
        if 'username' in session:
            user1 = session['username']
        user_id = models.User.query.filter_by(username1=user1).first();
        print(user_id)
        x = models.Medium.query.filter_by(UserId = user_id.id).all()
        print(x)
        for i in x:
            thing = models.Tvshows.query.get(i.id)
            print(thing)
            incomplete.append(thing)
        table = Results(incomplete)
        table.border = True
        return render_template('alltasks.html', table=table, title = "To-Do List")

@app.route('/register')
def register():
    return render_template('register.html')

@app.route("/logout")
def logout():
    session['logged_in'] = False
    return index()

@app.route("/location")
def location():
    return render_template("Locations.html")

@app.route('/login', methods=['POST'])
def do_admin_login():
    usernametemp = request.form['username']
    passwordtemp = request.form['password']
    temp = h = hashlib.md5(passwordtemp.encode())

    if  len(models.User.query.filter_by(username1=usernametemp,password1=h.hexdigest()).all()) == 1:
        session['logged_in'] = True
        session['username'] = usernametemp
    else:
        flash('incorrect username/password!')
    return index()

@app.route('/newuser', methods=['POST'])
def newuser():
    newusername = request.form['username']
    newuserpassword = request.form['password']
    h = hashlib.md5(newuserpassword.encode())

    if len(models.User.query.filter_by(username1=newusername).all()) == 0:
        x = models.User(username1=newusername, password1=h.hexdigest())
        db.session.add(x)
        db.session.commit()
        return redirect(url_for('index'))
    else:
        flash('User already exists')
    return render_template('register.html')

@app.route('/add', methods=['GET', 'POST'])
def create():
    user1 = "heyhey"
    if 'username' in session:
        user1 = session['username']
        x = models.User.query.filter_by(username1=user1).first()
    form = todoForm()
    print(user1)
    if form.validate_on_submit():
        todo = models.Tvshows(title=form.Title.data, Seasons=form.Seasons.data,
        Genre=form.Genre.data, Universe=form.Universe.data)
        db.session.add(todo)
        db.session.commit()
        y = models.Tvshows.query.filter_by(title=form.Title.data).first()
        print(x.id)
        new = models.Medium(UserId=x.id, TvshowsId=y.id)
        db.session.add(new)
        db.session.commit()
        return redirect(url_for('index'))
    return render_template('add.html', form=form)

@app.route('/alltasks')
def alltasks():
    return redirect(url_for('index'))

@app.route('/Locations')
def locations():
    return render_template("Locations.html")

@app.route('/search', methods=['GET', 'POST'])
def search():
    form = searchForm()
    l = []
    table1 = Results(0)
    if form.validate_on_submit():
        x = models.Tvshows.query.filter_by(Genre=form.Genre.data).all()

        user1 = "heyhey"
        if 'username' in session:
            user1 = session['username']
            y = models.User.query.filter_by(username1=user1).first()
        for i in x:
            l.append(i)
        table1 = Results(l)
        table1.border = True
        return render_template('alltasks.html', table=table1, form=form, title = "Search")
    return render_template('search.html', form=form)

@app.route('/settings', methods=['GET', 'POST'])
def settings():
        form = settingsForm()
        user1 = "heyhey"
        if 'username' in session:
            user1 = session['username']
        if form.validate_on_submit():
            usernametemp = request.form['usern']
            passwordtemp = request.form['passw']
            h = hashlib.md5(passwordtemp.encode())
            y = models.User.query.filter_by(username1=user1).first()
            y.username1 = usernametemp
            y.password1 = h.hexdigest()

        return render_template('settings.html', form=form)
